from lxml import etree
import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.crawler import CrawlerProcess
import json

class MySpider(CrawlSpider):
    name='innovate'
    allowed_domains = ['innovateperu.gob.pe']
    start_urls = ['https://www.innovateperu.gob.pe/convocatorias/por-tipo-de-concurso/concursos-para-empresas/220-concursos-para-microempresas',
                    'https://www.innovateperu.gob.pe/convocatorias/por-tipo-de-concurso/concursos-para-empresas/221-concursos-para-pequena-y-mediana-empresa',
                    'https://www.innovateperu.gob.pe/convocatorias/por-tipo-de-concurso/concursos-para-empresas/222-concursos-para-grande-empresa',]

    rules=(
    Rule(LinkExtractor(allow =(), restrict_xpaths = ('//h2[@itemprop="name"]/a')), callback = 'parse_item'),
    #Rule(LinkExtractor(allow =(), restrict_xpaths = ('//*[@class="span3"]/span')), callback = 'parse_start_url', follow = False),
    
    )

    # def parse_item(self, response):
    #     #response.xpath('normalize-space(//h1[@class="item-title__primary "]/text())').extract_first()
    #     nombre = response.xpath('//div[@class="page-header"]/h1/text()').extract()
    #     descripcion = response.xpath('//div[@class]/p[1]/text()').extract()
    #     yield {'NOMBRE': nombre,'DESCRIPCION': descripcion}


    def parse_item(self, response):
    #     nombre=response.xpath('//h2[@itemprop="name"]/a').extract()
    #     descripcion=response.xpath('//div[@class="span9"]/p').extract()
        urlBase='https://www.innovateperu.gob.pe'
        nombre = response.xpath('normalize-space(//div[@class="page-header"]/h1/text())').extract()
        descripcion = response.xpath('normalize-space(//*[@id="sp-component"]/div[2]/div[2]/p[1]/text())').extract()
        url=response.url
        #titulo = response.xpath('///title/text()').extract()
        # data = json.loads(response.body)
        # html = data['html']
        # selector = scrapy.Selector(text=data['html'], type="html")
        #sel=Selector(response)
        bases=[]
        for row in response.xpath('//table[@border=1]/tbody/tr'):
            name={
                'Convocatoria' : str(row.xpath('td[1]/text()').extract_first()),
                'Bases Preliminares' : urlBase+str(row.xpath('td[2]/a/@href').extract_first()),
                'Anexos Preliminares': urlBase + str(row.xpath('td[3]/a/@href').extract_first()),
                'Bases Integradas' : urlBase + str(row.xpath('td[4]/a/@href').extract_first()),
                'Anexos Integradas' : urlBase + str(row.xpath('td[5]/a/@href').extract_first()),
            }
            bases.append(name)
        result= {'BASES':bases,'NOMBRE': nombre,'DESCRIPCION': descripcion, 'URL': url}#, 'ESTADO': estado}
        print("RESULTADO",result)
    #     yield {'NOMBRE':nombre,'DESCRIPCION':descripcion}

# process=CrawlerProcess()
# process.crawl(MySpider)
# process.start()

def main(event,context):
    process=CrawlerProcess()
    process.crawl(MySpider)
    process.start()

def lambda_handler(event, context):
    # TODO implement
    print(__name__)
    print(etree.LXML_VERSION)
    main(None,None)

if __name__ == "__main__":
    lambda_handler(None, None)
